# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import pyodbc

# Some other example server values are
# server = 'localhost\sqlexpress' # for a named instance
# server = 'myserver,port' # to specify an alternate port
server = 'SRV-KZN-DB01' 
database = 'KznBonus' 
username = 'KORSTON\...' 
password = '...'  
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password + ";Trusted_Connection=yes;")
def h(bs):
    """Convert bytearray to hex string
 
    Given a bytearray, convert it to a string of
    an even length, containing the associated
    hexidecimal characters for each byte
    """
    try:
        hs = ["{0:0>2}".format(hex(b)[2:].upper()) for b in bs]
        return '0x' + ''.join(hs)
    except:
        return bs
cnxn.add_output_converter(pyodbc.SQL_BINARY, h)
cursor = cnxn.cursor()
# select n rows from SQL table to insert in dataframe.
query = """SELECT [timestamp]
      ,[AccountID]
      ,[FirstName]
      ,[LastName]
      ,[MiddleName]
      ,[Email]
      ,[MPhone]
      ,[PhoneValid]
      ,[Gender]
      ,[Birthday]
  FROM [KznBonus].[dbo].[ClubAccounts]
  where [Block] = 0 and [ElectronicCard] = 1"""
df = pd.read_sql(query, cnxn)
#print(df) 